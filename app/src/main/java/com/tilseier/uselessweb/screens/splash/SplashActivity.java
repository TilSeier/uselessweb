package com.tilseier.uselessweb.screens.splash;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.tilseier.uselessweb.R;
import com.tilseier.uselessweb.ads.AdmobApplication;
import com.tilseier.uselessweb.database.sqlite.AppDatabase;
import com.tilseier.uselessweb.utils.AppPreferences;
import com.tilseier.uselessweb.utils.NetworkConnection;

public class SplashActivity extends AppCompatActivity implements ISplash.View {

    FirebaseAnalytics mFirebaseAnalytics;

    ConstraintLayout clDialogErrorWrapper;
    Button btnDialogError;
    TextView tvDialogError;

    TextView tvLoadingSplash;

    //Animation
    Animation animScaleIn;
    Animation animScaleOut;

    ISplash.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Init AdMob
        AdmobApplication.createWallAd(this);
        AdmobApplication.requestNewInterstitial();

        AppDatabase db = Room.databaseBuilder(this, AppDatabase.class, "useless_web_db")
                .allowMainThreadQueries()
                .build();

        AppPreferences appPreferences = new AppPreferences(this);

        //Animation
        animScaleIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_in);
        animScaleOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_out);

        clDialogErrorWrapper = findViewById(R.id.layout_dialog_error_wrapper);
        btnDialogError = findViewById(R.id.btn_dialog_error);
        tvDialogError = findViewById(R.id.tv_dialog_error);

        tvLoadingSplash = findViewById(R.id.tv_loading_splash);

        presenter = new SplashPresenter(db, appPreferences, mFirebaseAnalytics, this);
        presenter.onCreate();

    }

    public void onDialogErrorButtonClick(View view){
        presenter.onDialogErrorButtonClick();
    }

    @Override
    public void goTo(Class goToClass) {
        Intent intent = new Intent(this, goToClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void setLoadingText(String str) {
        tvLoadingSplash.setText(str);
    }

    @Override
    public void showLoading() {
        tvLoadingSplash.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        tvLoadingSplash.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showDialogError(int strID) {
        tvDialogError.setText(strID);
        clDialogErrorWrapper.startAnimation(animScaleIn);
        clDialogErrorWrapper.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDialogError() {
        clDialogErrorWrapper.startAnimation(animScaleOut);
        clDialogErrorWrapper.setVisibility(View.GONE);
    }

    @Override
    public void showDialogErrorButton() {
        btnDialogError.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDialogErrorButton() {
        btnDialogError.setVisibility(View.GONE);
    }

    @Override
    public boolean isInternetConnection() {
        return NetworkConnection.networkAvailable(getApplicationContext());
    }
}
