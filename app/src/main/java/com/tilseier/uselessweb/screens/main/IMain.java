package com.tilseier.uselessweb.screens.main;

import java.util.ArrayList;

public interface IMain {

    interface View {

        void updateRecentlyList(ArrayList URLs, ArrayList titles, ArrayList iconURLs);
        void updateFavouritesList(ArrayList URLs, ArrayList titles, ArrayList iconURLs);
        void setCountOfRecentlyWeb(int count, int max);
        void setCountOfFavouritesWeb(int count, int max);
        void setDeleteDialogData(String title, String iconURL);
        void setFavouriteVideoDialogData(String title, String iconURL);
        void showDeleteDialog();
        void hideDeleteDialog();
        void showFavouriteVideoDialog();
        void hideFavouriteVideoDialog();
        void showRecentlyVideoDialog();
        void hideRecentlyVideoDialog();
        void showRateUsDialog();
        void hideRateUsDialog();
        void showWebMenu();
        void hideWebMenu();
        void showLoadingFragment(String title, String iconURL);
        void hideLoadingFragment();
        void showWebView();
        void hideWebView();
        void showPreLoader();
        void hidePreLoader();
        void checkFavourite();
        void uncheckFavourite();
        void showWebLoading();
        void hideWebLoading();
        void setWebName(String str);
        void showWebName();
        void hideWebName();
        void rotateRefreshIcon();
        void showTools();
        void hideTopBar();
        void deployWebName();
        void cutWebName();
        void loadWeb(String link);
        void fullScreen();
        void fullScreenExit();
        void refreshWeb();
        void setWebSound(boolean b);
        String[] getWebData();
        String[] getDeleteWebData();
        void shareWeb(String webURL);
        void showToast(String message);
        void loadRewardedVideoAd();
        void showRewardedVideoAd();
        boolean isInterstitialLoaded();
        void loadInterstitial();
        void showInterstitial();
        void goToRatePage();

    }

    interface Presenter {

        void onCreate();
        void onStart();
        void onSwitchWebClick();
        void onWebNameClick();
        void onLoadingWebHide();
        void onRefreshClick();
        void onMenuClick();
        void onFavouriteClick();
        void onMenuWebItemClick();
        void onRecentlySeeMoreItemClick();
        void onFavouriteDeleteClick();
        void onFavouriteDialogCloseClick();
        void onFavouriteDialogDeleteClick();
        void onFavouriteDialogWrapperClick();
        void onFavouriteVideoDialogCloseClick();
        void onFavouriteVideoDialogOkClick();
        void onFavouriteVideoDialogWrapperClick();
        void onRecentlyVideoDialogCloseClick();
        void onRecentlyVideoDialogOkClick();
        void onRecentlyVideoDialogWrapperClick();
        void onRateUsDialogRateClick();
        void onRateUsDialogRateLaterClick();
        void onRateUsDialogNoRateClick();
        void onFullscreenClick();
        void onShareWebClick();
        void onWebPageStarted();
        void onWebPageFinished();
        void onBackPressed();
        void onPause();
        void onResume();
        void onRewarded();
        void onRewardedVideoAdClosed();
        void onInterstitialAdClosed();

    }

    interface Model {

        void initLoadingLogic();

        String[] getWebData();

        void setWebData(String[] webData);

        ArrayList[] getRecentlyListData(int limit);

        ArrayList[] getFavouritesListData();

        boolean addToRecentlyList();

        void setNextWebPosition();

        boolean isFavourite();

        boolean addToFavourite();

        boolean deleteFromFavourite();

        boolean deleteFromFavouriteByURL(String webURL);

        int getCountOfFavouritesWeb();

        int getCountOfRecentlyWeb();

    }

}
