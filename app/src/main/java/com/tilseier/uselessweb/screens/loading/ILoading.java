package com.tilseier.uselessweb.screens.loading;

public interface ILoading {

    interface View {

        void setWebName(String webName);
        void setWebIcon(String webIconURL);
        void setLoadingText(String str);
        void pauseAd();
        void resumeAd();
        void destroyAd();

    }

    interface Presenter {

        void onCreate();
        void onStart();
        void onDetach();
        void onPause();
        void onResume();
        void onDestroy();

    }

}
