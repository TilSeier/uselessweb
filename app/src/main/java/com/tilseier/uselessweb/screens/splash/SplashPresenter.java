package com.tilseier.uselessweb.screens.splash;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.tilseier.uselessweb.database.sqlite.AppDatabase;
import com.tilseier.uselessweb.screens.main.MainActivity;
import com.tilseier.uselessweb.utils.AppPreferences;
import com.tilseier.uselessweb.utils.ShowLogic;

public class SplashPresenter implements ISplash.Presenter, ISplash.Model.Callback{

    private static final String TAG = "LogSplashPresenter";
//    private AppDatabase db;
    private ISplash.View view;
    private ISplash.Model model;
    private AppPreferences appPreferences;
    private FirebaseAnalytics mFirebaseAnalytics;
//    private static int SPLASH_TIME_OUT = 3000;

    private Handler dotAnimationTimer;
    private int dotAnimationTime = 0;

    private int ERROR_NO = 0;
    private int ERROR_INTERNET_CONNECTION = 1;
    private int ERROR_LOAD_DATA = 2;
    private int ERROR_STATUS = ERROR_NO;

    SplashPresenter(AppDatabase db, AppPreferences appPreferences, FirebaseAnalytics mFirebaseAnalytics, ISplash.View view){
        this.view = view;
        this.appPreferences = appPreferences;
        this.mFirebaseAnalytics = mFirebaseAnalytics;

        model = new SplashModel(db, appPreferences, this);

        dotAnimationTimer = new Handler();
    }

    @Override
    public void onCreate() {

        ERROR_STATUS = ERROR_NO;
        view.hideDialogError();
        view.hideDialogErrorButton();

        initApp();

    }

    @Override
    public void onDialogErrorButtonClick() {

        if (ERROR_STATUS == ERROR_INTERNET_CONNECTION){

            ERROR_STATUS = ERROR_NO;
            view.hideDialogError();
            view.hideDialogErrorButton();

            initApp();

        }

    }

    @Override
    public void onSuccess(int milliseconds) {

        Log.i(TAG, "M = "+milliseconds);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run()
            {

                dotAnimationTimer.removeCallbacksAndMessages(null);

                ShowLogic.startLogicTimer(true, appPreferences.isShowRate());

                view.goTo(MainActivity.class);

            }
        }, milliseconds);

        model.clear();

    }

    @Override
    public void onFailure(ISplash.Model.FailStatus status, String errorMessage) {
        //Failed to load data

//        dotAnimationTimer.removeCallbacksAndMessages(null);
//        view.hideLoading();
//        ERROR_STATUS = ERROR_LOAD_DATA;
//        view.hideDialogErrorButton();
//        view.showDialogError(R.string.error_load_data);


//        Log.i("LogSM Fill", "uW e.getMessage() = "+errorMessage);

        Bundle bundle = new Bundle();
        if (status == ISplash.Model.FailStatus.EMPTY_LIST) {
            bundle.putString("load_fail_status", "EMPTY_LIST");
        } else {
            bundle.putString("load_fail_status", "FIREBASE_FAIL");
        }
        bundle.putString("load_fail_error", ""+errorMessage);
        mFirebaseAnalytics.logEvent("failed_to_load_web_data", bundle);

    }

    private void initApp(){

        view.showLoading();

//        if(view.isInternetConnection()) {

            model.loadUselessWebToRoom();

            //TODO NUMBER RECENTLY AND FAVOURITES

            dotAnimationTimer.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (dotAnimationTime == 0) {
                        view.setLoadingText("Loading.\t\t\t");
                        dotAnimationTime = 1;
                    } else if (dotAnimationTime == 1) {
                        view.setLoadingText("Loading..\t\t");
                        dotAnimationTime = 2;
                    } else if (dotAnimationTime == 2) {
                        view.setLoadingText("Loading...\t");
                        dotAnimationTime = 0;
                    }

                    dotAnimationTimer.postDelayed(this, 500);

                }
            }, 500);

//        } else {
//
//            dotAnimationTimer.removeCallbacksAndMessages(null);
//            view.hideLoading();
//            ERROR_STATUS = ERROR_INTERNET_CONNECTION;
//            view.showDialogErrorButton();
//            view.showDialogError(R.string.error_internet_connection);
//
//        }

    }

}
