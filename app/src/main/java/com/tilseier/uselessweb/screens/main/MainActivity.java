package com.tilseier.uselessweb.screens.main;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.arch.persistence.room.Room;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tilseier.uselessweb.R;
import com.tilseier.uselessweb.ads.AdmobApplication;
import com.tilseier.uselessweb.database.sqlite.AppDatabase;
import com.tilseier.uselessweb.screens.loading.LoadingFragment;
import com.tilseier.uselessweb.utils.AppPreferences;
import com.tilseier.uselessweb.utils.FavouritesListRecyclerViewAdapter;
import com.tilseier.uselessweb.utils.NetworkConnection;
import com.tilseier.uselessweb.utils.RecentlyListRecyclerViewAdapter;
import com.tilseier.uselessweb.utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements IMain.View, FavouritesListRecyclerViewAdapter.ListListener, RecentlyListRecyclerViewAdapter.ListListener, RewardedVideoAdListener {

    final static String TAG = "GLOBALLOG";

    private FragmentManager fragmentManager;

    String currentHost = "";

    FirebaseAnalytics mFirebaseAnalytics;

    WebView mWebView;
    ImageView mFullscreen, mShareWeb, mFavourite, mRefreshIcon, mMenuIcon;
    ImageButton mSwitchWeb;
    TextView mWebName;
    LinearLayout mBottomBar;

    TextView tvCountOfRecentlyWeb;
    TextView tvCountOfFavouritesWeb;

    RecyclerView rvRecentlyList;
    RecyclerView rvFavouritesList;

    ConstraintLayout layoutWebLoading;
    ProgressBar pbWebLoading;

    ConstraintLayout layoutWebMenu;

    //Delete Dialog
    ConstraintLayout clDeleteDialog;
    CircleImageView ivDeleteIconWeb;
    ImageView ivDeleteClose;
    TextView tvDeleteWebTitle;
//    Button btnDeleteWeb;

    //Interstitial
    ConstraintLayout clPreLoader;

    //Watch Video Favourite Dialog
    ConstraintLayout clFavouriteVideoDialog;
    CircleImageView ivFavouriteVideoIconWeb;
    ImageView ivFavouriteVideoClose;
    TextView tvFavouriteVideoWebTitle;

    //Watch Video Recently Dialog
    ConstraintLayout clRecentlyVideoDialog;
    ImageView ivRecentlyVideoClose;

    //Rate Us Dialog
    ConstraintLayout clRateUsDialog;

    String[] webData;
    String[] deleteWebData;

    //Animation
    Animation animScaleIn;
    Animation animScaleOut;
    Animation animFadeIn;
    Animation animFadeOut;
    Animation animRotate360;

    //Rewarded Video Ad
    RewardedVideoAd mRewardedVideoAd;
    static final String rewardAdID = "ca-app-pub-7508352159228516/4026306275";//ca-app-pub-3940256099942544/5224354917

    IMain.Presenter presenter;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Interstitial
        AdmobApplication.createWallAd(this);
        AdmobApplication.requestNewInterstitial();

        //Rewarded Video Ad
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);

        AppDatabase db = Room.databaseBuilder(this, AppDatabase.class, "useless_web_db")
                .allowMainThreadQueries()
                .build();

        AppPreferences appPreferences = new AppPreferences(this);

        //Animation
        animScaleIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_in);
        animScaleOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_out);
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        animRotate360 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_360);

        mWebView = findViewById(R.id.wv_main_browser);
        mFullscreen = findViewById(R.id.iv_fullscreen);
        mWebName = findViewById(R.id.tv_web_name);
        mShareWeb = findViewById(R.id.iv_share_web);
        mRefreshIcon = findViewById(R.id.iv_refresh);
        mMenuIcon = findViewById(R.id.iv_menu);
        mSwitchWeb = findViewById(R.id.ib_switch_web);
        mBottomBar = findViewById(R.id.layout_bottom_bar);
        layoutWebLoading = findViewById(R.id.layout_web_loading);
        pbWebLoading = findViewById(R.id.pb_web_loading);
        mFavourite = findViewById(R.id.iv_favourite);

        tvCountOfRecentlyWeb = findViewById(R.id.tv_count_of_recently_web);
        tvCountOfFavouritesWeb = findViewById(R.id.tv_count_of_favourites_web);

        layoutWebMenu = findViewById(R.id.layout_web_menu);

        //Delete Dialog
        clDeleteDialog = findViewById(R.id.layout_dialog_delete_wrapper);
        ivDeleteIconWeb = findViewById(R.id.iv_web_icon_delete);
        ivDeleteClose = findViewById(R.id.iv_close_delete);
        tvDeleteWebTitle = findViewById(R.id.tv_web_name_delete);
//        btnDeleteWeb = findViewById(R.id.btn_delete_from_favourites);

        //Interstitial
        clPreLoader = findViewById(R.id.layout_preloader);

        //Watch Video Favourite Dialog
        clFavouriteVideoDialog = findViewById(R.id.layout_dialog_favourite_video_wrapper);
        ivFavouriteVideoIconWeb = findViewById(R.id.iv_web_icon_dialog_favourite_video);
        ivFavouriteVideoClose = findViewById(R.id.iv_close_dialog_favourite_video);
        tvFavouriteVideoWebTitle = findViewById(R.id.tv_web_name_dialog_favourite_video);

        //Watch Video Recently Dialog
        clRecentlyVideoDialog = findViewById(R.id.layout_dialog_recently_video_wrapper);
        ivRecentlyVideoClose = findViewById(R.id.iv_close_dialog_recently_video);

        //Rate Us Dialog
        clRateUsDialog = findViewById(R.id.layout_dialog_rate_us_wrapper);

        //RecycleView
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvRecentlyList = findViewById(R.id.rv_recently_web);
        rvRecentlyList.setLayoutManager(layoutManager);

        rvFavouritesList = findViewById(R.id.rv_favourites_web);
        rvFavouritesList.setLayoutManager(new LinearLayoutManager(this));


//        mSwitchWeb.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View arg0, MotionEvent arg1) {
//                mSwitchWeb.setSelected(arg1.getAction()==MotionEvent.ACTION_DOWN);
//                return true;
//            }
//        });

//        if (android.os.Build.VERSION.SDK_INT >= 21) {
//            CookieManager.getInstance().setAcceptThirdPartyCookies(mWebView, true);
//        }else {
//            CookieManager.getInstance().setAcceptCookie(true);
//        }


        //Web View Settings
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        mWebView.getSettings().setDomStorageEnabled(true);

//        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);

        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }

        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.setScrollbarFadingEnabled(false);

        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);

        mWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                presenter.onWebPageStarted();

                super.onPageStarted(view, url, favicon);
            }

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                final Uri uri = Uri.parse(url);
                return handleUri(uri);
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                final Uri uri = request.getUrl();
                return handleUri(uri);
            }

            private boolean handleUri(final Uri uri) {
                Log.i(TAG, "Uri =" + uri);
                final String host = uri.getHost();
//                final String scheme = uri.getScheme();
                Log.i(TAG, "Uri host =" + host);
                Log.i(TAG, "Uri currentHost =" + currentHost);

                // Based on some condition you need to determine if you are going to load the url
                // in your web view itself or in a browser.
                // You can use `host` or `scheme` or any part of the `uri` to decide.
                try {
                    if (host != null) {
                        if (host.equals(currentHost)) {
                            // Returning false means that you are going to load this url in the webView itself
                            return false;
                        } else {
                            // Returning true means that you need to handle what to do with the url
                            // e.g. open web page in a Browser
                            final Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                            return true;
                        }
                    }else {
                        return true;
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                    return true;
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
//                Toast.makeText(getApplicationContext(), "Web error, please reload the web", Toast.LENGTH_SHORT).show();

                super.onReceivedError(view, request, error);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                presenter.onWebPageFinished();

                super.onPageFinished(view, url);
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                pbWebLoading.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                mWebName.setText(title);
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
//                Log.i(TAG, ""+consoleMessage.message() + " -- From Line "
//                        + consoleMessage.lineNumber() + " of "
//                        + consoleMessage.sourceId());

                return super.onConsoleMessage(consoleMessage);
            }
        });


        presenter = new MainPresenter(db, appPreferences, mFirebaseAnalytics, this);
        presenter.onCreate();

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    public void onWebNameClick(View view){
        presenter.onWebNameClick();
    }

    public void onRefreshClick(View view){
        presenter.onRefreshClick();
    }

    public void onMenuClick(View view){
        presenter.onMenuClick();
    }

    public void onFavoriteClick(View view){
        presenter.onFavouriteClick();
    }

    public void onFullscreenClick(View view){
        presenter.onFullscreenClick();
    }

    public void onSwitchWebClick(View view){
        presenter.onSwitchWebClick();
    }

    public void onShareWebClick(View view){
        presenter.onShareWebClick();
    }

    public void onFavouriteDialogDeleteClick(View view){
        presenter.onFavouriteDialogDeleteClick();
    }

    public void onFavouriteDialogCloseClick(View view){
        presenter.onFavouriteDialogCloseClick();
    }

    public void onFavouriteDialogWrapperClick(View view){
        presenter.onFavouriteDialogWrapperClick();
    }

    public void onFavouriteVideoDialogOkClick(View view){
        presenter.onFavouriteVideoDialogOkClick();
    }

    public void onFavouriteVideoDialogCloseClick(View view){
        presenter.onFavouriteVideoDialogCloseClick();
    }

    public void onFavouriteVideoDialogWrapperClick(View view){
        presenter.onFavouriteVideoDialogWrapperClick();
    }

    public void onRecentlyVideoDialogOkClick(View view){
        presenter.onRecentlyVideoDialogOkClick();
    }

    public void onRecentlyVideoDialogCloseClick(View view){
        presenter.onRecentlyVideoDialogCloseClick();
    }

    public void onRecentlyVideoDialogWrapperClick(View view){
        presenter.onRecentlyVideoDialogWrapperClick();
    }

    public void onRateUsDialogRateClick(View view){
        presenter.onRateUsDialogRateClick();
    }

    public void onRateUsDialogNoRateClick(View view){
        presenter.onRateUsDialogNoRateClick();
    }

    public void onRateUsDialogRateLaterClick(View view){
        presenter.onRateUsDialogRateLaterClick();
    }

//    public void onRateUsDialogWrapperClick(View view){
//        presenter.onRateUsDialogWrapperClick();
//    }



    @Override
    public void updateRecentlyList(ArrayList URLs, ArrayList titles, ArrayList iconURLs) {
        RecentlyListRecyclerViewAdapter adapter = new RecentlyListRecyclerViewAdapter(this, this, URLs, titles, iconURLs);
        rvRecentlyList.setAdapter(adapter);
    }

    @Override
    public void updateFavouritesList(ArrayList URLs, ArrayList titles, ArrayList iconURLs) {

        FavouritesListRecyclerViewAdapter adapter = new FavouritesListRecyclerViewAdapter(this, this, URLs, titles, iconURLs);
        rvFavouritesList.setAdapter(adapter);

    }

    @Override
    public void setCountOfRecentlyWeb(int count, int max) {
        tvCountOfRecentlyWeb.setText(getString(R.string.text_count_of_web, count, max));
    }

    @Override
    public void setCountOfFavouritesWeb(int count, int max) {
        tvCountOfFavouritesWeb.setText(getString(R.string.text_count_of_web, count, max));
    }

    @Override
    public void setDeleteDialogData(String title, String iconURL) {
        tvDeleteWebTitle.setText(title);
        Glide.with(getApplicationContext())
                .load(iconURL)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_useless_web)
                        .error(R.drawable.ic_useless_web)
                )
                .into(ivDeleteIconWeb);
    }

    @Override
    public void setFavouriteVideoDialogData(String title, String iconURL) {
        tvFavouriteVideoWebTitle.setText(title);
        Glide.with(getApplicationContext())
                .load(iconURL)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_useless_web)
                        .error(R.drawable.ic_useless_web)
                )
                .into(ivFavouriteVideoIconWeb);
    }

    @Override
    public void showDeleteDialog() {
        clDeleteDialog.startAnimation(animScaleIn);
        clDeleteDialog.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDeleteDialog() {
        clDeleteDialog.startAnimation(animScaleOut);
        clDeleteDialog.setVisibility(View.GONE);
    }

    @Override
    public void showFavouriteVideoDialog() {
        clFavouriteVideoDialog.startAnimation(animScaleIn);
        clFavouriteVideoDialog.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFavouriteVideoDialog() {
        clFavouriteVideoDialog.startAnimation(animScaleOut);
        clFavouriteVideoDialog.setVisibility(View.GONE);
    }

    @Override
    public void showRecentlyVideoDialog() {
        clRecentlyVideoDialog.startAnimation(animScaleIn);
        clRecentlyVideoDialog.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRecentlyVideoDialog() {
        clRecentlyVideoDialog.startAnimation(animScaleOut);
        clRecentlyVideoDialog.setVisibility(View.GONE);
    }

    @Override
    public void showRateUsDialog() {
        clRateUsDialog.startAnimation(animScaleIn);
        clRateUsDialog.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRateUsDialog() {
        clRateUsDialog.startAnimation(animScaleOut);
        clRateUsDialog.setVisibility(View.GONE);
    }

    @Override
    public void showWebMenu() {
        layoutWebMenu.startAnimation(animFadeIn);
        layoutWebMenu.setVisibility(View.VISIBLE);
        mMenuIcon.setImageResource(R.drawable.ic_close_menu_24dp);
    }

    @Override
    public void hideWebMenu() {
        layoutWebMenu.startAnimation(animFadeOut);
        layoutWebMenu.setVisibility(View.GONE);
        mMenuIcon.setImageResource(R.drawable.ic_menu_black_24dp);
    }

    @Override
    public void showLoadingFragment(String title, String iconURL) {

        Fragment LoadingFragment = fragmentManager
                .findFragmentByTag(Utils.LoadingFragment);

        if (LoadingFragment != null){
            fragmentManager
                    .beginTransaction()
                    .remove(LoadingFragment)
                    .commit();
        }

        Bundle bundle = new Bundle();
        bundle.putString("webTitle", title);
        bundle.putString("webIconURL", iconURL);
        // set Fragmentclass Arguments
        Fragment loadingFragment = new LoadingFragment();
        loadingFragment.setArguments(bundle);

        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.right_enter, R.anim.left_out, R.anim.right_enter, R.anim.left_out)
                .replace(R.id.frameContainer, loadingFragment,
                        Utils.LoadingFragment)
                .addToBackStack(null)
                .commit();

//        if (LoadingFragment != null) {
//
//            fragmentManager
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.right_enter, R.anim.left_out)
//                    .show(LoadingFragment)
//                    .commit();
//
//        }else {

//        }

    }

    @Override
    public void hideLoadingFragment() {

        Log.i(TAG, "hideLoadingFragment");

        Fragment LoadingFragment = fragmentManager
                .findFragmentByTag(Utils.LoadingFragment);

        if (LoadingFragment != null){

            Log.i(TAG, "LoadingFragment exist");

            try {

                if (!LoadingFragment.isHidden()) {

                    fragmentManager
                            .beginTransaction()
                            .setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .hide(LoadingFragment)
                            .commit();

                    Log.i(TAG, "HIDE");
                    presenter.onLoadingWebHide();

                }
//                fragmentManager.popBackStack();

            }catch (IllegalStateException e){
                e.printStackTrace();

//                fragmentManager.beginTransaction().remove(LoadingFragment).commit();

                Log.i(TAG, "onException");

            }

        }

    }

    @Override
    public void showWebView() {
        mWebView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWebView() {
        mWebView.setVisibility(View.GONE);
    }

    @Override
    public void showPreLoader() {
        clPreLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePreLoader() {
        clPreLoader.setVisibility(View.GONE);
    }

    @Override
    public void checkFavourite() {
        mFavourite.setImageResource(R.drawable.ic_favorite_black_24dp);
    }

    @Override
    public void uncheckFavourite() {
        mFavourite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
    }

    @Override
    public void showWebLoading() {
        layoutWebLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWebLoading() {
        layoutWebLoading.setVisibility(View.GONE);
    }

    @Override
    public void setWebName(String str) {
        mWebName.setText(str);
    }

    @Override
    public void showWebName() {
        mWebName.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWebName() {
        mWebName.setVisibility(View.GONE);
    }

    @Override
    public void rotateRefreshIcon() {
        mRefreshIcon.startAnimation(animRotate360);
    }

    @Override
    public void showTools() {
        mFullscreen.setVisibility(View.VISIBLE);
        mWebName.setVisibility(View.VISIBLE);
        mSwitchWeb.setVisibility(View.VISIBLE);
        mShareWeb.setVisibility(View.VISIBLE);
        mBottomBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTopBar() {
        mFullscreen.setVisibility(View.GONE);
        mWebName.setVisibility(View.GONE);
    }

    @Override
    public void deployWebName() {
        mWebName.setMaxLines(Integer.MAX_VALUE);
    }

    @Override
    public void cutWebName() {
        mWebName.setMaxLines(1);
    }

    @Override
    public void loadWeb(String link) {

        if (!link.equals("about:blank")) {
            URL url;
            try {
                url = new URL(link);
                currentHost = url.getHost();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }else {
            currentHost = "Blank";
        }
        mWebView.loadUrl(link);

    }

    @Override
    public void fullScreen() {
        mWebName.setVisibility(View.GONE);
        mSwitchWeb.setVisibility(View.GONE);
        mShareWeb.setVisibility(View.GONE);
        mBottomBar.setVisibility(View.GONE);
        mFullscreen.setImageResource(R.drawable.ic_fullscreen_exit_black_24dp);
    }

    @Override
    public void fullScreenExit() {
        mFullscreen.setVisibility(View.VISIBLE);
        mWebName.setVisibility(View.VISIBLE);
        mSwitchWeb.setVisibility(View.VISIBLE);
        mShareWeb.setVisibility(View.VISIBLE);
        mBottomBar.setVisibility(View.VISIBLE);
        mFullscreen.setImageResource(R.drawable.ic_fullscreen_black_24dp);
    }

    @Override
    public void refreshWeb() {
        mWebView.reload();
    }

    @Override
    public void setWebSound(boolean b) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            mWebView.getSettings().setMediaPlaybackRequiresUserGesture(!b);//false
//        }
    }

    @Override
    public String[] getWebData() {
        return webData;
    }

    @Override
    public String[] getDeleteWebData() {
        return deleteWebData;
    }

    @Override
    public void shareWeb(String webURL) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getString(R.string.shared_web_message, webURL);
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Useless Web");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadRewardedVideoAd() {
        if (!mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.loadAd(rewardAdID,
                    new AdRequest.Builder()
//                            .addTestDevice("EC07F4759620B8F1E3BD5F493490BEB4")
                            .build());
//.addTestDevice("EC07F4759620B8F1E3BD5F493490BEB4")
//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        }
    }

    @Override
    public void showRewardedVideoAd() {
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        } else {
            loadRewardedVideoAd();
            if (!NetworkConnection.networkAvailable(getApplicationContext()))
                showToast("No internet connection");
            else
                showToast("Please, try again later");
        }
    }

    @Override
    public boolean isInterstitialLoaded() {
        return AdmobApplication.isAdLoaded();
    }

    @Override
    public void loadInterstitial() {
        AdmobApplication.requestNewInterstitial();
    }

    @Override
    public void showInterstitial() {

        if (AdmobApplication.isAdLoaded()){

            AdmobApplication.displayLoadedAd();

            AdmobApplication.mInterstitialAd.setAdListener(new AdListener(){
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    AdmobApplication.requestNewInterstitial();

                    presenter.onInterstitialAdClosed();

                    Log.i("GLOBALAD", "onAdClosed");
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();

                    Log.i("GLOBALAD", "onAdLeftApplication");
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    Log.i("GLOBALAD", "onAdOpened");
                }

            });

        }else{

            AdmobApplication.requestNewInterstitial();
        }

    }

    @Override
    public void goToRatePage() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=com.tilseier.uselessweb")));
        }catch (ActivityNotFoundException e){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.tilseier.uselessweb")));
        }
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onFavouriteItemClick(String[] webData) {
        this.webData = webData;
        presenter.onMenuWebItemClick();
    }

    @Override
    public void onFavouriteDeleteClick(String[] webData) {
        this.deleteWebData = webData;
        presenter.onFavouriteDeleteClick();
    }

    @Override
    public void onRecentlyItemClick(String[] webData) {
        this.webData = webData;
        presenter.onMenuWebItemClick();
    }

    @Override
    public void onRecentlySeeMoreItemClick() {
        presenter.onRecentlySeeMoreItemClick();
    }

    //Rewarded Video Ads

    @Override
    public void onRewardedVideoAdLoaded() {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        presenter.onRewardedVideoAdClosed();
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        presenter.onRewarded();
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }

    @Override
    public void onRewardedVideoCompleted() {

    }
}
