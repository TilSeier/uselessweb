package com.tilseier.uselessweb.screens.loading;

import android.os.Handler;

public class LoadingPresenter implements ILoading.Presenter {

    private ILoading.View view;

    private String[] arguments;

    private Handler dotAnimationTimer;
    private int dotAnimationTime = 0;

    LoadingPresenter(String[] arguments, ILoading.View view){
        this.view = view;
        this.arguments = arguments;

        dotAnimationTimer = new Handler();
    }

    @Override
    public void onCreate() {
        view.setWebIcon(arguments[1]);
        view.setWebName(arguments[0]);

        dotAnimationTimer.postDelayed(new Runnable(){
            @Override
            public void run()
            {

                if (dotAnimationTime == 0){
                    view.setLoadingText("Loading.\t\t");
                    dotAnimationTime = 1;
                } else if (dotAnimationTime == 1) {
                    view.setLoadingText("Loading..\t\t");
                    dotAnimationTime = 2;
                } else if (dotAnimationTime == 2) {
                    view.setLoadingText("Loading...\t");
                    dotAnimationTime = 0;
                }

                dotAnimationTimer.postDelayed(this, 500);

            }
        }, 500);

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onDetach() {
        dotAnimationTimer.removeCallbacksAndMessages(null);
    }

    @Override
    public void onPause() {
        view.pauseAd();
    }

    @Override
    public void onResume() {
        view.resumeAd();
    }

    @Override
    public void onDestroy() {
        view.destroyAd();
    }

}
