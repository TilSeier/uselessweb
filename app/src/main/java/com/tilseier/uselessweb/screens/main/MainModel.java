package com.tilseier.uselessweb.screens.main;

import com.tilseier.uselessweb.database.sqlite.AppDatabase;
import com.tilseier.uselessweb.database.sqlite.FavouriteWeb;
import com.tilseier.uselessweb.database.sqlite.RecentlyWeb;
import com.tilseier.uselessweb.database.sqlite.UselessWeb;
import com.tilseier.uselessweb.utils.LoadingLogic;

import java.util.ArrayList;
import java.util.List;

public class MainModel implements IMain.Model {

    private AppDatabase db;
    private int webID = -1;
    private String[] args;

    public static int DB_RECENTLY_WEB_LIMIT = 50;

    MainModel(AppDatabase db){
        this.db = db;
        args = new String[3];
    }

    @Override
    public void initLoadingLogic() {
        if (!LoadingLogic.isIsLogicInit()) {
            LoadingLogic.initLoadingLogic(db.uselessWebDao().getNumberOfUselessWeb());
        }
    }

    @Override
    public String[] getWebData() {
//        String[] args = new String[3];

        try {
            webID = LoadingLogic.getWebID();
            UselessWeb uselessWeb = db.uselessWebDao().getUselessWeb(webID);
            args[0] = uselessWeb.getUrl();
            args[1] = uselessWeb.getTitle();
            args[2] = uselessWeb.getIcon();
        }catch (Exception e){
            webID = -1;
            e.printStackTrace();
            args[0] = "about:blank";
            args[1] = "Blank";
            args[2] = "";
        }

        return args;
    }

    @Override
    public void setWebData(String[] webData) {
        webID = -2;
        args[0] = webData[0];
        args[1] = webData[1];
        args[2] = webData[2];
    }

    @Override
    public ArrayList[] getRecentlyListData(int limit) {
        ArrayList[] recentlyArraysData = new ArrayList[3];

        ArrayList<String> recentlyWebURLs = new ArrayList<>();
        ArrayList<String> recentlyWebTitles = new ArrayList<>();
        ArrayList<String> recentlyWebIconURLs = new ArrayList<>();

        List<RecentlyWeb> recentlyWebList = db.recentlyWebDao().getRecentlyWeb(limit);

        for (RecentlyWeb recentlyWebItem : recentlyWebList){

            recentlyWebURLs.add(recentlyWebItem.getUrl());
            recentlyWebIconURLs.add(recentlyWebItem.getIcon());
            recentlyWebTitles.add(recentlyWebItem.getTitle());

        }

        recentlyArraysData[0] = recentlyWebURLs;
        recentlyArraysData[1] = recentlyWebTitles;
        recentlyArraysData[2] = recentlyWebIconURLs;

        return recentlyArraysData;
    }

    @Override
    public ArrayList[] getFavouritesListData() {

        ArrayList[] favouritesArraysData = new ArrayList[3];

        ArrayList<String> favouriteWebURLs = new ArrayList<>();
        ArrayList<String> favouriteWebTitles = new ArrayList<>();
        ArrayList<String> favouriteWebIconURLs = new ArrayList<>();

        List<FavouriteWeb> favouriteWebList = db.favouriteWebDao().getFavouriteWeb();

        for (FavouriteWeb favouriteWebItem : favouriteWebList){

            favouriteWebURLs.add(favouriteWebItem.getUrl());
            favouriteWebTitles.add(favouriteWebItem.getTitle());
            favouriteWebIconURLs.add(favouriteWebItem.getIcon());

        }

        favouritesArraysData[0] = favouriteWebURLs;
        favouritesArraysData[1] = favouriteWebTitles;
        favouritesArraysData[2] = favouriteWebIconURLs;

        return favouritesArraysData;

    }

    @Override
    public boolean addToRecentlyList() {
        if (webID != -1){

            try {

                //TODO UPDATE IF EXISTS

                db.recentlyWebDao().deleteRecentlyWebByURL(args[0]);

//                UselessWeb uselessWeb = db.uselessWebDao().getUselessWeb(webID);
                RecentlyWeb recentlyWeb = new RecentlyWeb();
                recentlyWeb.setUrl(args[0]);
                recentlyWeb.setTitle(args[1]);
                recentlyWeb.setIcon(args[2]);

                db.recentlyWebDao().addRecentlyWeb(recentlyWeb);

                if (db.recentlyWebDao().getCountOfRecentlyWeb() > DB_RECENTLY_WEB_LIMIT){
                    db.recentlyWebDao().deleteRecentlyWebToLimit(DB_RECENTLY_WEB_LIMIT);
                }

                return true;

            }catch (Exception e){
                e.printStackTrace();
                return false;
            }

        }
        return false;
    }

    @Override
    public void setNextWebPosition() {
        LoadingLogic.setNextWebPosition();
    }

    @Override
    public boolean isFavourite() {
        if (webID != -1){

            try {

                return db.favouriteWebDao().getNumberByURL(args[0]) != 0;

            }catch (Exception e){
                e.printStackTrace();
                return false;
            }

        }
        return false;
    }

    @Override
    public boolean addToFavourite() {
        if (webID != -1){

            try {

//                UselessWeb uselessWeb = db.uselessWebDao().getUselessWeb(webID);
                FavouriteWeb favouriteWeb = new FavouriteWeb();
                favouriteWeb.setUrl(args[0]);
                favouriteWeb.setTitle(args[1]);
                favouriteWeb.setIcon(args[2]);

                db.favouriteWebDao().addFavouriteWeb(favouriteWeb);

                return true;

            }catch (Exception e){
                e.printStackTrace();
                return false;
            }

        }
        return false;
    }

    @Override
    public boolean deleteFromFavourite() {
        if (webID != -1){

            try {

                db.favouriteWebDao().deleteFavouriteWeb(args[0]);

                return true;

            }catch (Exception e){
                e.printStackTrace();
                return false;
            }

        }
        return false;
    }

    @Override
    public boolean deleteFromFavouriteByURL(String webURL) {
        try {

            db.favouriteWebDao().deleteFavouriteWeb(webURL);

            return true;

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int getCountOfFavouritesWeb() {
        return db.favouriteWebDao().getCountOfFavouritesWeb();
    }

    @Override
    public int getCountOfRecentlyWeb() {
        return db.recentlyWebDao().getCountOfRecentlyWeb();
    }
}
