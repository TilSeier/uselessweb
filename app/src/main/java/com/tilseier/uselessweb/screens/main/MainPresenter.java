package com.tilseier.uselessweb.screens.main;

import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.tilseier.uselessweb.database.sqlite.AppDatabase;
import com.tilseier.uselessweb.utils.AppPreferences;
import com.tilseier.uselessweb.utils.ShowLogic;

import java.util.ArrayList;

public class MainPresenter implements IMain.Presenter {

    private IMain.View view;

    private IMain.Model model;

    private AppDatabase db;
    private AppPreferences appPreferences;
    private FirebaseAnalytics mFirebaseAnalytics;

    private boolean isFullscreen = false;
    private boolean isMenuOpened = false;
    private boolean isWebNameDeployed = false;
    private boolean isDeleteDialogOpened = false;
    private boolean isFavouriteVideoDialogOpened = false;
    private boolean isRecentlyVideoDialogOpened = false;
    private boolean isRateUsDialogOpened = false;
    private boolean isRateWasShown = false;
    private boolean isRateWasClicked = false;

    private boolean doCloseRateDialog = false;

    private int NO_REWARD = 0;
    private int FAVOURITE_REWARD = 1;
    private int RECENTLY_REWARD = 2;
    private int REWARD = NO_REWARD;

    private int LOADING_TIMER = 4000;

    private Handler loadingTimer;
    private Handler loadingInterstitialTimer;

    private String currentWebURL = "http://spaceis.cool";
    private String currentWebTitle = "Blank";
    private String currentWebIconURL = "";

    private boolean isLoadingTimerRun;

    MainPresenter(AppDatabase db, AppPreferences appPreferences, FirebaseAnalytics mFirebaseAnalytics, IMain.View view){
        this.view = view;
        this.appPreferences = appPreferences;
        this.mFirebaseAnalytics = mFirebaseAnalytics;
        model = new MainModel(db);
    }

    @Override
    public void onCreate() {

        view.loadRewardedVideoAd();

        loadingTimer = new Handler();
        loadingInterstitialTimer = new Handler();

        view.hidePreLoader();

        view.hideRateUsDialog();
        isRateUsDialogOpened = false;
        isRateWasShown = false;
        isRateWasClicked = false;

        view.hideDeleteDialog();
        isDeleteDialogOpened = false;

        view.hideFavouriteVideoDialog();
        isFavouriteVideoDialogOpened = false;

        view.hideRecentlyVideoDialog();
        isRecentlyVideoDialogOpened = false;

        view.hideWebMenu();
        isMenuOpened = false;

        view.fullScreenExit();
        isFullscreen = false;

        view.cutWebName();
        isWebNameDeployed = false;

        view.uncheckFavourite();

        updateCountOfRecentlyWeb();
        updateCountOfFavouritesWeb();

        model.initLoadingLogic();

        ShowLogic.startLogicTimer(true, appPreferences.isShowRate());

        ArrayList[] favouriteArraysData = model.getFavouritesListData();
        view.updateFavouritesList(favouriteArraysData[0], favouriteArraysData[1], favouriteArraysData[2]);

        loadNextWeb();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onSwitchWebClick() {

        if (!isLoadingTimerRun) {

            if (appPreferences.isShowRate() && !isRateWasClicked && ShowLogic.isShowRate()) {

                view.showRateUsDialog();
                isRateUsDialogOpened = true;

                isRateWasShown = true;

            } else {
                loadNextWeb();
            }

        }

    }

    @Override
    public void onWebNameClick() {

        toggleWebName();

    }

    @Override
    public void onLoadingWebHide() {
        view.fullScreenExit();
//        view.hideWebName();
        view.setWebName(currentWebTitle);

        view.cutWebName();
        isWebNameDeployed = false;
        isFullscreen = false;

        if (model.isFavourite()){
            view.checkFavourite();
        }else {
            view.uncheckFavourite();
        }

        view.loadWeb(currentWebURL);

        model.addToRecentlyList();

    }

    @Override
    public void onRefreshClick() {
        if (!isLoadingTimerRun) {
            if (ShowLogic.isShowAds())
                showInterstitial();

//            view.refreshWeb();
            view.loadWeb(currentWebURL);
        }
        view.rotateRefreshIcon();
        if (isMenuOpened) {
            view.hideWebMenu();
            isMenuOpened = false;
        }
    }

    @Override
    public void onMenuClick() {
        //OPEN FAVOURITE

        if (isMenuOpened){
            view.hideWebMenu();
            isMenuOpened = false;
        }else {
            view.showWebMenu();
            isMenuOpened = true;
        }

        if (!isLoadingTimerRun) {
            if (ShowLogic.isShowAds())
                showInterstitial();
        }

    }

    @Override
    public void onFavouriteClick() {
        if (!isLoadingTimerRun) {
            //ADD TO FAVOURITE

            if (model.isFavourite()) {

                if (model.deleteFromFavourite())
                    view.uncheckFavourite();

                if (ShowLogic.isShowAds())
                    showInterstitial();

            } else {

                if (model.getCountOfFavouritesWeb() < appPreferences.getFavouriteWebLimit()) {

                    if (model.addToFavourite())
                        view.checkFavourite();

                }else {
                    view.setFavouriteVideoDialogData(currentWebTitle, currentWebIconURL);

                    view.showFavouriteVideoDialog();
                    isFavouriteVideoDialogOpened = true;
                }
            }
            ArrayList[] favouriteArraysData = model.getFavouritesListData();
            view.updateFavouritesList(favouriteArraysData[0], favouriteArraysData[1], favouriteArraysData[2]);

            updateCountOfFavouritesWeb();

        }
    }

    @Override
    public void onMenuWebItemClick() {
        if (!isLoadingTimerRun) {
            loadMenuItemWeb();
        }else {
            view.hideWebMenu();
            isMenuOpened = false;
            view.showToast("Please, wait until previous web is loading");
        }
    }

    @Override
    public void onRecentlySeeMoreItemClick() {
        view.showRecentlyVideoDialog();
        isRecentlyVideoDialogOpened = true;
    }

    @Override
    public void onFavouriteDeleteClick() {

        String[] webData = view.getDeleteWebData();

        view.setDeleteDialogData(webData[1], webData[2]);

        view.showDeleteDialog();
        isDeleteDialogOpened = true;

    }

    @Override
    public void onFavouriteDialogCloseClick() {
        view.hideDeleteDialog();
        isDeleteDialogOpened = false;
    }

    @Override
    public void onFavouriteDialogDeleteClick() {

        view.hideDeleteDialog();
        isDeleteDialogOpened = false;

        String[] webData = view.getDeleteWebData();

        if (currentWebURL.equals(webData[0])){
            if (model.isFavourite()) {
                if (model.deleteFromFavourite())
                    view.uncheckFavourite();
            }
        }else {

            model.deleteFromFavouriteByURL(webData[0]);

        }
        ArrayList[] favouriteArraysData = model.getFavouritesListData();
        view.updateFavouritesList(favouriteArraysData[0], favouriteArraysData[1], favouriteArraysData[2]);
        updateCountOfFavouritesWeb();

    }

    @Override
    public void onFavouriteDialogWrapperClick() {
        view.hideDeleteDialog();
        isDeleteDialogOpened = false;
    }

    @Override
    public void onFavouriteVideoDialogCloseClick() {
        view.hideFavouriteVideoDialog();
        isFavouriteVideoDialogOpened = false;
    }

    @Override
    public void onFavouriteVideoDialogOkClick() {
        //SHOW VIDEO
//        view.showToast("VIDEO");
        REWARD = FAVOURITE_REWARD;
        view.showRewardedVideoAd();

        view.hideFavouriteVideoDialog();
        isFavouriteVideoDialogOpened = false;

    }

    @Override
    public void onFavouriteVideoDialogWrapperClick() {
        view.hideFavouriteVideoDialog();
        isFavouriteVideoDialogOpened = false;
    }

    @Override
    public void onRecentlyVideoDialogCloseClick() {
        view.hideRecentlyVideoDialog();
        isRecentlyVideoDialogOpened = false;
    }

    @Override
    public void onRecentlyVideoDialogOkClick() {
        //SHOW VIDEO
        int recentlyWebLimit = appPreferences.getRecentlyWebLimit();

        if (recentlyWebLimit < MainModel.DB_RECENTLY_WEB_LIMIT) {
            REWARD = RECENTLY_REWARD;
            view.showRewardedVideoAd();
        } else {
            view.showToast("Recently web limit is reached");
        }

        view.hideRecentlyVideoDialog();
        isRecentlyVideoDialogOpened = false;

    }

    @Override
    public void onRecentlyVideoDialogWrapperClick() {
        view.hideRecentlyVideoDialog();
        isRecentlyVideoDialogOpened = false;
    }

    @Override
    public void onRateUsDialogRateClick() {
        appPreferences.setShowRate(false);

        ShowLogic.setShowRate(false);
        isRateWasClicked = true;

        view.goToRatePage();
        view.showToast("Thank you very much :)");
        doCloseRateDialog = true;
        isRateUsDialogOpened = false;

        Bundle bundle = new Bundle();
        bundle.putString("rate_app_counter", "count_of_clicks");
        mFirebaseAnalytics.logEvent("rate_app", bundle);
    }

    @Override
    public void onRateUsDialogRateLaterClick() {

        ShowLogic.setShowRate(false);
        ShowLogic.startLogicTimer(true, appPreferences.isShowRate());

        view.hideRateUsDialog();
        isRateUsDialogOpened = false;
    }

    @Override
    public void onRateUsDialogNoRateClick() {
        appPreferences.setShowRate(false);

        ShowLogic.setShowRate(false);
//        ShowLogic.startLogicTimer(true, appPreferences.isShowRate());

        view.hideRateUsDialog();
        isRateUsDialogOpened = false;
    }

    @Override
    public void onFullscreenClick() {

        toggleFullScreen();

        if (ShowLogic.isShowAds())
            showInterstitial();

//        if (isFullscreen){
//            view.fullScreenExit();
//            isFullscreen = false;
//        }else {
//            view.fullScreen();
//            isFullscreen = true;
//        }

    }

    @Override
    public void onShareWebClick() {
        if (!isLoadingTimerRun) {
            //SHARE CURRENT WEB

            view.shareWeb(currentWebURL);

        }
    }

    @Override
    public void onWebPageStarted() {

        if (!isLoadingTimerRun) {
            view.showWebLoading();
            view.hideWebView();
//            view.hideWebName();
        }

//        layoutWebLoading.setVisibility(View.VISIBLE);
//        mWebView.setVisibility(View.GONE);
//        mWebName.setVisibility(View.GONE);

    }

    @Override
    public void onWebPageFinished() {

        if (!isLoadingTimerRun) {
            view.hideWebLoading();
            view.showWebView();
//            view.showWebName();
        }

//        layoutWebLoading.setVisibility(View.GONE);
//        mWebView.setVisibility(View.VISIBLE);
//        mWebName.setVisibility(View.VISIBLE);

    }

    @Override
    public void onBackPressed() {
        if (isFavouriteVideoDialogOpened){
            view.hideFavouriteVideoDialog();
            isFavouriteVideoDialogOpened = false;
        }else if (isRecentlyVideoDialogOpened){
            view.hideRecentlyVideoDialog();
            isRecentlyVideoDialogOpened = false;
        }else if (isDeleteDialogOpened){
            view.hideDeleteDialog();
            isDeleteDialogOpened = false;
        }else if (isMenuOpened){
            view.hideWebMenu();
            isMenuOpened = false;
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

        if (!isLoadingTimerRun){

            view.hideLoadingFragment();

        }

        if (doCloseRateDialog){
            view.hideRateUsDialog();
            doCloseRateDialog = false;
        }

    }

    @Override
    public void onRewarded() {
        if (REWARD == FAVOURITE_REWARD){

//            appPreferences.setFavouriteWebLimit(appPreferences.getFavouriteWebLimit()+1);

            if (model.isFavourite()) {
                if (model.deleteFromFavourite())
                    view.uncheckFavourite();
            } else {

                if (model.addToFavourite())
                    view.checkFavourite();

            }
            ArrayList[] favouriteArraysData = model.getFavouritesListData();
            view.updateFavouritesList(favouriteArraysData[0], favouriteArraysData[1], favouriteArraysData[2]);

            updateCountOfFavouritesWeb();


            Bundle bundle = new Bundle();
            bundle.putString("web_title", currentWebTitle);
            bundle.putString("web_icon_url", currentWebIconURL);
            bundle.putString("web_url", currentWebURL);
            bundle.putString("web_favourite_count", ""+model.getCountOfFavouritesWeb());
            mFirebaseAnalytics.logEvent("favourite_video_reward", bundle);


            REWARD = NO_REWARD;
        }else if(REWARD == RECENTLY_REWARD){

            appPreferences.setRecentlyWebLimit(appPreferences.getRecentlyWebLimit()+1);

            int recentlyWebLimit = appPreferences.getRecentlyWebLimit();

            ArrayList[] recentlyArraysData = model.getRecentlyListData(recentlyWebLimit);
            view.updateRecentlyList(recentlyArraysData[0], recentlyArraysData[1], recentlyArraysData[2]);
            updateCountOfRecentlyWeb();


            Bundle bundle = new Bundle();
            bundle.putString("recently_limit", ""+recentlyWebLimit);
            mFirebaseAnalytics.logEvent("recently_video_reward", bundle);


            REWARD = NO_REWARD;
        }
        view.loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdClosed() {
        view.loadRewardedVideoAd();
    }

    @Override
    public void onInterstitialAdClosed() {
        ShowLogic.setShowAds(false);
        ShowLogic.startLogicTimer(true, appPreferences.isShowRate());
    }

    private void loadNextWeb(){

        isLoadingTimerRun = true;

        if (isMenuOpened) {
            view.hideWebMenu();
            isMenuOpened = false;
        }

        view.hideTopBar();

        ArrayList[] recentlyArraysData = model.getRecentlyListData(appPreferences.getRecentlyWebLimit());
        view.updateRecentlyList(recentlyArraysData[0], recentlyArraysData[1], recentlyArraysData[2]);
        updateCountOfRecentlyWeb();

        String[] webData = model.getWebData();
        model.setNextWebPosition();

        currentWebURL = webData[0];
        currentWebTitle = webData[1];
        currentWebIconURL = webData[2];


        view.showLoadingFragment(currentWebTitle, currentWebIconURL);

        loadingTimer.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (ShowLogic.isShowAds())
                    showInterstitial();

                view.loadWeb("about:blank");

            }
        }, 1000);

        loadingTimer.postDelayed(new Runnable() {
            @Override
            public void run() {

                isLoadingTimerRun = false;

                view.hideLoadingFragment();

            }
        }, LOADING_TIMER);

    }

    private void loadMenuItemWeb(){

        isLoadingTimerRun = true;

        if (isMenuOpened) {
            view.hideWebMenu();
            isMenuOpened = false;
        }

        view.hideTopBar();

        String[] webData = view.getWebData();
        model.setWebData(webData);

        ArrayList[] recentlyArraysData = model.getRecentlyListData(appPreferences.getRecentlyWebLimit());
        view.updateRecentlyList(recentlyArraysData[0], recentlyArraysData[1], recentlyArraysData[2]);
        updateCountOfRecentlyWeb();

        currentWebURL = webData[0];
//        String webTitle = webData[1];
//        String webIconURL = webData[2];
        currentWebTitle = webData[1];
        currentWebIconURL = webData[2];

        view.showLoadingFragment(currentWebTitle, currentWebIconURL);

        loadingTimer.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (ShowLogic.isShowAds())
                    showInterstitial();

                view.loadWeb("about:blank");

            }
        }, 1000);

        loadingTimer.postDelayed(new Runnable() {
            @Override
            public void run() {

                isLoadingTimerRun = false;

                view.hideLoadingFragment();

            }
        }, LOADING_TIMER);

    }

    private void toggleWebName(){

        if (isWebNameDeployed){
            view.cutWebName();
            isWebNameDeployed = false;
        }else {
            view.deployWebName();
            isWebNameDeployed = true;
        }

    }

    private void toggleFullScreen(){

        if (isFullscreen){
            view.fullScreenExit();
            isFullscreen = false;
        }else {
            view.fullScreen();
            isFullscreen = true;
        }

    }

    private void updateCountOfRecentlyWeb(){
        int countOfRecentlyWeb = model.getCountOfRecentlyWeb();
        int recentlyWebLimit = appPreferences.getRecentlyWebLimit();

        if (countOfRecentlyWeb > recentlyWebLimit)
            countOfRecentlyWeb = recentlyWebLimit;

        view.setCountOfRecentlyWeb(countOfRecentlyWeb, recentlyWebLimit);
    }

    private void updateCountOfFavouritesWeb(){
        int countOfFavouritesWeb = model.getCountOfFavouritesWeb();
        int favouritesWebLimit = appPreferences.getFavouriteWebLimit();

//        if (countOfFavouritesWeb > favouritesWebLimit)
//            countOfFavouritesWeb = favouritesWebLimit;

        view.setCountOfFavouritesWeb(countOfFavouritesWeb, favouritesWebLimit);
    }

    private void showInterstitial(){

        view.showPreLoader();

        loadingInterstitialTimer.postDelayed(new Runnable() {
            @Override
            public void run() {

                view.showInterstitial();

                view.hidePreLoader();

            }
        }, 1000);

    }

}
