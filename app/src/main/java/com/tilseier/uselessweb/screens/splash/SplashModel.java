package com.tilseier.uselessweb.screens.splash;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.tilseier.uselessweb.database.sqlite.AppDatabase;
import com.tilseier.uselessweb.database.sqlite.UselessWeb;
import com.tilseier.uselessweb.utils.AppPreferences;
import com.tilseier.uselessweb.utils.HardcodedUselessWebData;
import com.tilseier.uselessweb.utils.LoadingLogic;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SplashModel implements ISplash.Model{

    private static final String TAG = "LogSplashModel";

    private AppDatabase db;
    private ISplash.Model.Callback callback;

    private Disposable disposable;

    private FirebaseFirestore mFirebaseFirestore;

    private UselessWeb uselessWeb;

    private AppPreferences appPreferences;

    private final long secToUpdate = 3600*24*7;//week //3600*24//day //60//minute
    private boolean isRoomFilled;
    private boolean isTimeToUpdate;

    SplashModel(AppDatabase db, AppPreferences appPreferences, ISplash.Model.Callback callback){
        this.db = db;
        this.callback = callback;
        this.appPreferences = appPreferences;

        isRoomFilled = false;
        isTimeToUpdate = false;

        uselessWeb = new UselessWeb();
        mFirebaseFirestore = FirebaseFirestore.getInstance();
    }

    @Override
    public void loadUselessWebToRoom() {
        disposable = Single.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return initApp();//fillAndUpdateRoom();//fillFirestoreList();
            }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object myObject) throws Exception {

                if ((boolean)myObject){
                    callback.onSuccess(3000);
                }
//                updateUi(myObject);
//                callback.onSuccess();
            }
        });

    }

    @Override
    public void clear() {
        if (disposable != null)
            disposable.dispose();
    }


    private boolean initApp() {

        isTimeToUpdate = ((System.currentTimeMillis() - appPreferences.getLastUpdateTimeMillis()) / 1000) >= secToUpdate;

        isRoomFilled = appPreferences.isRoomFilled() && db.uselessWebDao().getNumberOfUselessWeb() > 150;

        if (!isRoomFilled || isTimeToUpdate) {//is Room is filled and updated

            fillAndUpdateRoom();

            return false;

        } else {

//            Log.i(TAG, "ALREADY_UPDATED");

            return true;

        }

    }



    private boolean fillAndUpdateRoom() {

        mFirebaseFirestore.collection("uselessWeb").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        if (queryDocumentSnapshots.isEmpty()){

                            //EMPTY

                            if (db.uselessWebDao().getNumberOfUselessWeb() > 150){

//                                Log.i(TAG, "UPDATED_EMPTY");

                                callback.onSuccess(1000);
                            } else {
//                                appPreferences.setRoomFilled(false);

//                                Log.i(TAG, "UPDATED_EMPTY_FILL");

                                callback.onFailure(FailStatus.EMPTY_LIST, "EMPTY_LIST");
                                fillRoomFromHardcode();

                            }

//                            return;
                        } else {

//                            Log.i("LogSplashModel", ""+queryDocumentSnapshots.getDocuments());

                            db.uselessWebDao().deleteAllUselessWeb();

                            List<DocumentSnapshot> uselessWebMaps = queryDocumentSnapshots.getDocuments();

                            int webIndex = 1;
                            for (DocumentSnapshot documentSnapshot: uselessWebMaps){

//                                Log.i("LogSM webTitle", ""+documentSnapshot.getString("webTitle"));
//                                Log.i("LogSM webIconURL", ""+documentSnapshot.getString("webIconURL"));
//                                Log.i("LogSM webURL", ""+documentSnapshot.getString("webURL"));

                                uselessWeb.setId(webIndex);
                                uselessWeb.setTitle(documentSnapshot.getString("webTitle"));
                                uselessWeb.setIcon(documentSnapshot.getString("webIconURL"));
                                uselessWeb.setUrl(documentSnapshot.getString("webURL"));

                                db.uselessWebDao().addUselessWeb(uselessWeb);

                                webIndex++;

                            }

                            LoadingLogic.initLoadingLogic(db.uselessWebDao().getNumberOfUselessWeb());

                            appPreferences.setRoomFilled(true);
                            appPreferences.setLastUpdateTimeMillis(System.currentTimeMillis());


//                            Log.i(TAG, "UPDATED_SUCCESS "+db.uselessWebDao().getNumberOfUselessWeb());
//                            Log.i(TAG, "UPDATED_SUCCESS webIndex="+webIndex);
//                            Log.i(TAG, "UPDATED_SUCCESS uselessWebMaps="+uselessWebMaps.size());

                            callback.onSuccess(1000);

                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

//                        appPreferences.setRoomFilled(false);

//                        Log.i(TAG, "UPDATED_addOnFailureListener");

                        if (db.uselessWebDao().getNumberOfUselessWeb() > 150){
                            callback.onSuccess(1000);
                        } else {

//                            appPreferences.setRoomFilled(false);
                            callback.onFailure(FailStatus.FIREBASE_FAIL, e.getMessage());
                            fillRoomFromHardcode();

                        }

                    }
                });

        return true;

    }

    private void fillRoomFromHardcode() {

        String[][] uselessWebData = HardcodedUselessWebData.getAllUselessWeb();

//        Log.i("LogSM Fill", "uW length = "+uselessWebData.length);

        db.uselessWebDao().deleteAllUselessWeb();

        int webIndex = 1;
        for (final String[] uW: uselessWebData) {

            uselessWeb.setId(webIndex);
            uselessWeb.setTitle(uW[0]);
            uselessWeb.setIcon(uW[1]);
            uselessWeb.setUrl(uW[2]);

            db.uselessWebDao().addUselessWeb(uselessWeb);

            webIndex++;

        }
//        Log.i("LogSM Fill", "webIndex = "+webIndex);


        callback.onSuccess(1000);

    }

}
