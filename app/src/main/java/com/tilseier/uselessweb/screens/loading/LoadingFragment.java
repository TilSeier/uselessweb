package com.tilseier.uselessweb.screens.loading;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tilseier.uselessweb.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoadingFragment extends Fragment implements ILoading.View {

    Context context;
    View view;

    TextView tvWebName;
    TextView tvLoading;
    CircleImageView ivWebIcon;
    ImageView ivWebBackground;

    AdView mAdView;
    static AdRequest adRequest;

    ILoading.Presenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_loading, container, false);
        initLoading();
        return view;
    }

    private void initLoading() {

        context = getActivity();

        //banner view
        LinearLayout layoutAdWrapper = view.findViewById(R.id.layout_ad_wrapper);
        layoutAdWrapper.setVisibility(View.VISIBLE);

        mAdView = view.findViewById(R.id.adViewLoading);

        if (adRequest == null) {
            adRequest = new AdRequest.Builder()
//                    .addTestDevice("EC07F4759620B8F1E3BD5F493490BEB4")
                    .build();
        }
        mAdView.loadAd(adRequest);
//        if (mAdView.isActivated()){
//            layoutAdWrapper.setVisibility(View.VISIBLE);
//        }

        assert getArguments() != null;
        String[] loadingArguments = new String[2];
        loadingArguments[0] = getArguments().getString("webTitle");
        loadingArguments[1] = getArguments().getString("webIconURL");

        tvWebName = view.findViewById(R.id.tv_loading_web_name);
        tvLoading = view.findViewById(R.id.tv_loading);
        ivWebIcon = view.findViewById(R.id.iv_loading_web_icon);
        ivWebBackground = view.findViewById(R.id.iv_loading_web_background);

        presenter = new LoadingPresenter(loadingArguments, this);
        presenter.onCreate();

    }

    @Override
    public void setWebName(String webName) {
        tvWebName.setText(webName);
    }

    @Override
    public void setWebIcon(String webIconURL) {

        if (!webIconURL.isEmpty()) {
            Glide.with(context)
                    .load(webIconURL)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_useless_web)
                            .error(R.drawable.ic_useless_web)
                    )
                    .into(ivWebIcon);

            Glide.with(context)
                    .load(webIconURL)
                    .apply(new RequestOptions()
                            .override(3, 3)
                            .placeholder(R.drawable.bg_1)
                            .error(R.drawable.bg_1)
                    )
                    .into(ivWebBackground);

        }

    }

    @Override
    public void setLoadingText(String str) {
        tvLoading.setText(str);
    }

    @Override
    public void pauseAd() {
        if (mAdView != null) {
            mAdView.pause();
        }
    }

    @Override
    public void resumeAd() {
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void destroyAd() {
        if (mAdView != null) {
            mAdView.destroy();
        }
    }

    @Override
    public void onDetach() {
        presenter.onDetach();
        super.onDetach();
    }

    @Override
    public void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        presenter.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
