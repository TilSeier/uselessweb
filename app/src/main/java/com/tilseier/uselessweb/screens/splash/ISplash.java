package com.tilseier.uselessweb.screens.splash;

public interface ISplash {

    interface View{

        void goTo(Class goToClass);
        void setLoadingText(String str);
        void showLoading();
        void hideLoading();
        void showDialogError(int strID);
        void hideDialogError();
        void showDialogErrorButton();
        void hideDialogErrorButton();
        boolean isInternetConnection();

    }

    interface Presenter{

        void onCreate();
        void onDialogErrorButtonClick();

    }

    interface Model{

        enum FailStatus {EMPTY_LIST, FIREBASE_FAIL}

        interface Callback{
            void onSuccess(int milliseconds);
            void onFailure(FailStatus status, String errorMessage);
        }

        void loadUselessWebToRoom();
        void clear();

    }

}
