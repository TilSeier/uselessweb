package com.tilseier.uselessweb.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tilseier.uselessweb.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 1/1/2018.
 */

public class FavouritesListRecyclerViewAdapter extends RecyclerView.Adapter<FavouritesListRecyclerViewAdapter.ViewHolder>{

    private static final String TAG = "FavouritesListAdapter";

    private ArrayList<String> webURLs = new ArrayList<>();
    private ArrayList<String> webTitles = new ArrayList<>();
    private ArrayList<String> webIconURLs = new ArrayList<>();
    private Context mContext;
    private FavouritesListRecyclerViewAdapter.ListListener listener;

    public FavouritesListRecyclerViewAdapter(Context context, FavouritesListRecyclerViewAdapter.ListListener listener, ArrayList<String> webURLs, ArrayList<String> webTitles, ArrayList<String> webIconURLs ) {
        this.webURLs = webURLs;
        this.webTitles = webTitles;
        this.webIconURLs = webIconURLs;
        mContext = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_favourites_listitem, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        if (!webIconURLs.get(position).isEmpty()) {
            Glide.with(mContext)
                    .asBitmap()
                    .load(webIconURLs.get(position))
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_useless_web)
                            .error(R.drawable.ic_useless_web)
                    )
                    .into(holder.image);
        }

        holder.imageName.setText(webTitles.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " + webTitles.get(position));

                String[] webData = new String[3];
                webData[0] = webURLs.get(position);
                webData[1] = webTitles.get(position);
                webData[2] = webIconURLs.get(position);

                listener.onFavouriteItemClick(webData);
            }
        });

        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] webData = new String[3];
                webData[0] = webURLs.get(position);
                webData[1] = webTitles.get(position);
                webData[2] = webIconURLs.get(position);

                listener.onFavouriteDeleteClick(webData);

            }
        });

    }

    @Override
    public int getItemCount() {
        return webTitles.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView imageName;
        ImageView imageDelete;
        ConstraintLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.iv_favourites_icon);
            imageName = itemView.findViewById(R.id.tv_favourites_title);
            parentLayout = itemView.findViewById(R.id.cl_favourite_item);
            imageDelete = itemView.findViewById(R.id.iv_favourite_delete);
        }
    }

    public interface ListListener{

        void onFavouriteItemClick(String[] webData);
        void onFavouriteDeleteClick(String[] webData);

    }
}















