package com.tilseier.uselessweb.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnection {

//    static NetworkConnection instance;
//
//    private NetworkConnection (){
//    }
//
//    public static NetworkConnection getInstance(){
//        if(null==instance)
//            instance = new NetworkConnection();
//        return instance;
//    }


    public static boolean networkAvailable(Context context) {
        try {
            boolean wifiDataAvailable = false;
            boolean mobileDataAvailable = false;
            ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] networkInfo = conManager != null ? conManager.getAllNetworkInfo() : new NetworkInfo[0];//TODO MAYBE CHANGE
            for (NetworkInfo netInfo : networkInfo) {
                if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                    if (netInfo.isConnected())
                        wifiDataAvailable = true;
                if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (netInfo.isConnected())
                        mobileDataAvailable = true;
            }
            return wifiDataAvailable || mobileDataAvailable;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
