package com.tilseier.uselessweb.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {

    private String INT_RECENTLY_WEB_LIMIT = "recently_web_limit";
    private String INT_FAVOURITES_WEB_LIMIT = "favourites_web_limit";
    private String BOOLEAN_IS_ROOM_FILLED_AND_UPDATED = "is_room_filled_and_updated";
    private String LONG_LAST_UPDATE_TIME = "last_update_time";
    private String BOOLEAN_SHOW_RATE = "show_rate_dialog";

    private SharedPreferences sPref;
    private SharedPreferences.Editor prefEditor;

    public AppPreferences(Context context){
        sPref = context.getSharedPreferences("com.tilseier.uselessweb", Context.MODE_PRIVATE);
        prefEditor = sPref.edit();
    }

    public boolean isRoomFilled(){
        return sPref.getBoolean(BOOLEAN_IS_ROOM_FILLED_AND_UPDATED, false);
    }

    public void setRoomFilled(boolean b){
        prefEditor.putBoolean(BOOLEAN_IS_ROOM_FILLED_AND_UPDATED, b)
                .apply();
    }

    public long getLastUpdateTimeMillis(){
        return sPref.getLong(LONG_LAST_UPDATE_TIME, 0);
    }

    public void setLastUpdateTimeMillis(long milliseconds){
        prefEditor.putLong(LONG_LAST_UPDATE_TIME, milliseconds)
                .apply();
    }

    public int getRecentlyWebLimit(){
        return sPref.getInt(INT_RECENTLY_WEB_LIMIT, 3);
    }

    public void setRecentlyWebLimit(int limit){
        prefEditor.putInt(INT_RECENTLY_WEB_LIMIT, limit)
                .apply();
    }

    public int getFavouriteWebLimit(){
        return sPref.getInt(INT_FAVOURITES_WEB_LIMIT, 3);
    }

    public void setFavouriteWebLimit(int limit){
        prefEditor.putInt(INT_FAVOURITES_WEB_LIMIT, limit)
                .apply();
    }

    public boolean isShowRate(){
        return sPref.getBoolean(BOOLEAN_SHOW_RATE, true);
    }

    public void setShowRate(boolean b){
        prefEditor.putBoolean(BOOLEAN_SHOW_RATE, b)
                .apply();
    }

}
