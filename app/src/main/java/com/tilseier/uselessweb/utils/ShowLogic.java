package com.tilseier.uselessweb.utils;

import android.os.Handler;

public class ShowLogic {

    private static boolean isShowAds = false;
    private static boolean isShowRate = false;
    private static boolean isAdTimerStarted = false;
    private static boolean isRateTimerStarted = false;
    private static int AD_TIMER = 60000 * 3;
    private static int RATE_TIMER = 60000 * 8;

    public static void startLogicTimer(boolean startAdTimer, boolean startRateTimer){

        if (startAdTimer) {

            if (!isAdTimerStarted) {
                isAdTimerStarted = true;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isShowAds = true;
                        isAdTimerStarted = false;

//                        if (!isShowRate) {
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    isShowRate = true;//!isShowRate
//                                }
//                            }, 1000);//8000
//                        }

                    }
                }, AD_TIMER);
            }

        }

        if (startRateTimer) {

            if (!isRateTimerStarted) {
                isRateTimerStarted = true;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        isShowRate = true;
                        isRateTimerStarted = false;

                    }
                }, RATE_TIMER);
            }

        }

    }

    public static boolean isShowAds(){
        return isShowAds;
    }

    public static void setShowAds(boolean b){
        isShowAds = b;
    }

    public static boolean isShowRate(){
        return isShowRate;
    }

    public static void setShowRate(boolean b){
        isShowRate = b;
    }

}
