package com.tilseier.uselessweb.utils;

import java.util.Arrays;
import java.util.Collections;

public class LoadingLogic {

    private static int CURRENT_POSITION = 0;
    private static int AMOUNT_OF_WEB = 0;
    private static Integer[] SHUFFLED_WEB_ID;

    private static boolean isLogicInit = false;

//    public static int getCurrentPosition() {
//        return CURRENT_POSITION;
//    }


    public static boolean isIsLogicInit() {
        return isLogicInit;
    }

    public static void initLoadingLogic(int amountOfWeb){

        if (!isLogicInit){

            AMOUNT_OF_WEB = amountOfWeb;
            CURRENT_POSITION = 0;
            shuffleWeb();

            isLogicInit = true;
        }

    }

    public static void setNextWebPosition() {

        int currentPosition = CURRENT_POSITION + 1;

        if (currentPosition < AMOUNT_OF_WEB) {
            CURRENT_POSITION = currentPosition;
        } else {
            CURRENT_POSITION = 0;
            shuffleWeb();
        }

    }

//    public static int getAmountOfWeb() {
//        return AMOUNT_OF_WEB;
//    }

//    public static void setAmountOfWeb(int amountOfWeb) {
//        AMOUNT_OF_WEB = amountOfWeb;
//    }

    public static Integer getWebID() {
//        try {
        return SHUFFLED_WEB_ID[CURRENT_POSITION];
//        }catch (Exception e){
//            e.printStackTrace();
//            return 0;
//        }
    }

    private static void shuffleWeb() {
        SHUFFLED_WEB_ID = getShuffledWeb();
    }

    private static Integer[] getShuffledWeb() {
        Integer[] shuffledID = new Integer[AMOUNT_OF_WEB];
        for (int i = 0; i < shuffledID.length; i++) {
            shuffledID[i] = i + 1;
        }
        Collections.shuffle(Arrays.asList(shuffledID));
        System.out.println(Arrays.toString(shuffledID));

        return shuffledID;
    }

}
