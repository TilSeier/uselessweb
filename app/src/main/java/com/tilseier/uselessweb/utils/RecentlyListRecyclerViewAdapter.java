package com.tilseier.uselessweb.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tilseier.uselessweb.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by TilSeier on 27/07/2018.
 */

public class RecentlyListRecyclerViewAdapter extends RecyclerView.Adapter<RecentlyListRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecentlyListAdapter";

    //vars
    private ArrayList<String> webURLs = new ArrayList<>();
    private ArrayList<String> webTitles = new ArrayList<>();
    private ArrayList<String> webIconURLs = new ArrayList<>();
    private Context mContext;
    private RecentlyListRecyclerViewAdapter.ListListener listener;

    public RecentlyListRecyclerViewAdapter(Context context, RecentlyListRecyclerViewAdapter.ListListener listener, ArrayList<String> webURLs, ArrayList<String> webTitles, ArrayList<String> webIconURLs) {
        this.webURLs = webURLs;
        this.webTitles = webTitles;
        this.webIconURLs = webIconURLs;
        mContext = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        if(viewType == R.layout.layout_recently_listitem){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recently_listitem, parent, false);
        }

        else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recently_see_more_listitem, parent, false);
        }

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder: called.");

        if(position != webURLs.size()) {

            if (!webIconURLs.get(position).isEmpty()) {
                Glide.with(mContext)
                        .asBitmap()
                        .load(webIconURLs.get(position))
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_useless_web)
                                .error(R.drawable.ic_useless_web)
                        )
                        .into(holder.image);
            }

            holder.name.setText(webTitles.get(position));

            holder.clParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: clicked on an image: " + webTitles.get(position));

                    String[] webData = new String[3];
                    webData[0] = webURLs.get(position);
                    webData[1] = webTitles.get(position);
                    webData[2] = webIconURLs.get(position);

                    listener.onRecentlyItemClick(webData);


                }
            });

        } else {

            holder.clSeeMoreParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    listener.onRecentlySeeMoreItemClick();

                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return webIconURLs.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == webIconURLs.size()) ? R.layout.layout_recently_see_more_listitem : R.layout.layout_recently_listitem;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView name;
        ConstraintLayout clParent;
        ConstraintLayout clSeeMoreParent;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.iv_recently_icon);
            name = itemView.findViewById(R.id.tv_recently_title);
            clParent = itemView.findViewById(R.id.cl_recently_item);
            clSeeMoreParent = itemView.findViewById(R.id.cl_recently_see_more_item);
        }
    }

    public interface ListListener{

        void onRecentlyItemClick(String[] webData);

        void onRecentlySeeMoreItemClick();

    }

}
