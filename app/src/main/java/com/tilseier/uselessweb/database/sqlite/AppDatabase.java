package com.tilseier.uselessweb.database.sqlite;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {UselessWeb.class, FavouriteWeb.class, RecentlyWeb.class}, version = 1)//, exportSchema = false
public abstract class AppDatabase extends RoomDatabase {

    public abstract UselessWebDao uselessWebDao();

    public abstract FavouriteWebDao favouriteWebDao();

    public abstract RecentlyWebDao recentlyWebDao();

}
