package com.tilseier.uselessweb.database.sqlite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface FavouriteWebDao {

    @Insert
    void addFavouriteWeb(FavouriteWeb favouriteWeb);

    @Query("select * from favourite_web ORDER BY id DESC")
    List<FavouriteWeb> getFavouriteWeb();

    @Query("DELETE FROM favourite_web WHERE web_url = :webURL")
    void deleteFavouriteWeb(String webURL);

    @Query("SELECT COUNT(id) FROM favourite_web WHERE web_url = :webURL")
    int getNumberByURL(String webURL);

    @Query("SELECT COUNT(id) FROM favourite_web")
    int getCountOfFavouritesWeb();

}
