package com.tilseier.uselessweb.database.sqlite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface UselessWebDao {

    @Insert
    void addUselessWeb(UselessWeb uselessWeb);

    @Query("SELECT * FROM useless_web WHERE id = :id")
    UselessWeb getUselessWeb(int id);

    @Query("DELETE FROM useless_web")
    void deleteAllUselessWeb();

    @Query("SELECT COUNT(id) FROM useless_web")
    int getNumberOfUselessWeb();

}
