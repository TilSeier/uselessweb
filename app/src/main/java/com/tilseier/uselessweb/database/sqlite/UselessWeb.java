package com.tilseier.uselessweb.database.sqlite;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "useless_web")
public class UselessWeb {

    @PrimaryKey
    private int id;

    @ColumnInfo(name = "web_title")
    private String title;

    @ColumnInfo(name = "web_icon")
    private String icon;

    @ColumnInfo(name = "web_url")
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}