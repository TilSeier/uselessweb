package com.tilseier.uselessweb.database.sqlite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface RecentlyWebDao {

    @Insert
    void addRecentlyWeb(RecentlyWeb recentlyWeb);

    @Query("select * from recently_web ORDER BY id DESC LIMIT :recentlyLimit")
    List<RecentlyWeb> getRecentlyWeb(int recentlyLimit);

    @Query("DELETE FROM recently_web WHERE web_url = :webURL")
    void deleteRecentlyWebByURL(String webURL);

    @Query("SELECT COUNT(id) FROM recently_web")
    int getCountOfRecentlyWeb();

    @Query("DELETE FROM recently_web where id NOT IN (SELECT id from recently_web ORDER BY id DESC LIMIT :recentlyLimit)")
    void deleteRecentlyWebToLimit(int recentlyLimit);

//    @Query("DELETE FROM recently_web ORDER BY id LIMIT 1")//TODO DELETE LAST RECORD
    @Delete
    void deleteRecentlyWeb(RecentlyWeb recentlyWeb);

}
