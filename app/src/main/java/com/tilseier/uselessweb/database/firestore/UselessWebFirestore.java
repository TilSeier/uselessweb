package com.tilseier.uselessweb.database.firestore;

public class UselessWebFirestore {

    String webTitle;
    String webIconURL;
    String webURL;

    public String getWebTitle() {
        return webTitle;
    }

    public void setWebTitle(String webTitle) {
        this.webTitle = webTitle;
    }

    public String getWebIconURL() {
        return webIconURL;
    }

    public void setWebIconURL(String webIconURL) {
        this.webIconURL = webIconURL;
    }

    public String getWebURL() {
        return webURL;
    }

    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }

}
